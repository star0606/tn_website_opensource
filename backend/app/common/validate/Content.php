<?php


namespace app\common\validate;


class Content extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:150',
        'main_image' => 'require|max:255',
        'category_id' => 'require|number',
        'model_id' => 'require|number|gt:0',
        'table_id' => 'require|number|gt:0',
        'view_count' => 'require|number',
        'like_count' => 'require|number',
        'share_count' => 'require|number',
        'sort' => 'require|number|gt:0',
        'recomm' => 'number|between:0,1',
        'status' => 'number|between:0,1',
        'type' => 'require',
        'limit' => 'require|number',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '内容标题不能为空',
        'title.max' => '内容标题最大长度为150',
        'main_image.require' => '主图不能为空',
        'main_image.max' => '主图最大长度为255',
        'category_id.require' => '所属栏目不能为空',
        'category_id.number' => '所属栏目值不正确',
        'model_id.require' => '所属模型不能为空',
        'model_id.number' => '所属模型值不正确',
        'model_id.gt' => '所属模型值不正确',
        'table_id.require' => '所属表id不能为空',
        'table_id.number' => '所属表id值不正确',
        'table_id.gt' => '所属表id值不正确',
        'view_count.require' => '浏览次数不能为空',
        'view_count.number' => '浏览次数输入的格式不正确',
        'like_count.require' => '点赞次数不能为空',
        'like_count.number' => '点赞次数输入的格式不正确',
        'share_count.require' => '分享次数不能为空',
        'share_count.number' => '分享次数输入的格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序值不正确',
        'sort.gt' => '排序值不正确',
        'recomm.number' => '是否推荐值格式不正确',
        'recomm.between' => '是否推荐值格式不正确',
        'status.number' => '推荐值格式不正确',
        'status.between' => '推荐值格式不正确',
        'type.require' => '获取推荐内容类型不能为空',
        'limit.require' => '获取推荐内容的数量不能为空',
        'limit.number' => '获取推荐内容的数量值格式不正确',
    ];

    protected $scene = [
        'add' => ['title','category_id','model_id','view_count','like_count','share_count','sort','recomm','status'],
        'edit' => ['id','title','category_id','model_id','table_id','view_count','like_count','share_count','sort','recomm','status'],
        'recomm' => ['type','limit'],
        'newest' => ['type','limit'],
        'paginate' => ['type','category_id'],
        'data' => ['id','type'],
        'operation' => ['id','type'],
    ];
}