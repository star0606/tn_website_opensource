<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 12:17
 */
declare(strict_types = 1);

use think\facade\Route;

Route::group(function () {
    Route::group(':app/:version', function () {

        // 轮播图管理
        Route::group('banner', function () {
            Route::get(':pos_id/:limit',':app\:version\Banner@getBanner'); // 根据pos_id获取轮播图数据
        });

        // 栏目管理
        Route::group('category', function () {
            Route::get('get_nav/:top_id',':app\:version\Category@getNavData');
        });

        // 配置参数管理
        Route::group('config/wx', function () {
            Route::get('/',':app\:version\WxConfig@getConfigValue');
        });

        // 搜索管理
        Route::group('search', function () {
            Route::get('get_result',':app\:version\Search@getList');
        });

        // 业务管理
        Route::group('business', function () {
            Route::get('get_title/:limit',':app\:version\Business@getBusinessTitle');
            Route::get('get_id/:id',':app\:version\Business@getBusinessData');
            Route::post('update_advisory/:id',':app\:version\Business@updateAdvisoryUser')->middleware('check.business.operation');
        });

        // 内容管理
        Route::group('content', function () {
            Route::get('get_category_list',':app\:version\Content@getCategoryPaginate');
            Route::get('get_recomm/:type/:limit',':app\:version\Content@getRecommData');
            Route::get('get_newest/:type/:limit',':app\:version\Content@getNewestData');
            Route::get('get_data/:type/:id',':app\:version\Content@getContentData');
            Route::post('update_operation/:type/:id', ':app\:version\Content@updateOperationUser')->middleware('check.content.operation');
        });

        // Token令牌管理
        Route::group('token', function () {
            Route::post('get',':app\:version\Token@getToken');    // 获取token令牌
            Route::post('verify',':app\:version\Token@verifyToken');  // 验证token令牌
        });

        // 用户管理
        Route::group('user', function () {
            Route::put('update',':app\:version\User@updateUserInfo'); // 更新用户信息
        });
    });
})->prefix('\\app\\api\\controller\\')
    ->allowCrossDomain([
        'Access-Control-Allow-Headers' => 'Token, Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With',
    ]);