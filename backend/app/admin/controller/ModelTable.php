<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-28
 * Time: 16:05
 */

namespace app\admin\controller;



use app\admin\BaseController;
use app\admin\model\ModelTable as ModelTableModel;

class ModelTable extends BaseController
{
    /**
     * 获取模型分页数据
     * @http get
     * @url /model_table/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = ModelTableModel::getPaginationList($params);

        return tn_yes('获取模型分页数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取模型信息数据
     * @http get
     * @url /model_table/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = ModelTableModel::getModelTableByID($id);

        return tn_yes('获取模型信息数据成功', ['data' => $data]);
    }

    /**
     * 获取全部模型名称
     * @http get
     * @url /model_table/get_all_name
     * @return \think\response\Json
     */
    public function getAllName()
    {
        $data = ModelTableModel::getAllModelTableName();

        return tn_yes('获取全部模型名称成功', ['data' => $data]);
    }

    /**
     * 获取模型字段数据
     * @http get
     * @url /model_table/get_fields_data
     * @return \think\response\Json
     */
    public function getFieldsData()
    {
        $params = $this->request->get(['id'=>0,'table_id'=>0]);

        $data = ModelTableModel::getFieldsData($params);

        return tn_yes('获取模型字段数据成功', ['data' => $data]);
    }

    /**
     * 添加模型信息
     * @http post
     * @url /model_table/add
     * @return \think\response\Json
     */
    public function addModelTable()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['cn_name','en_name','status','fields_data']);

        $result = ModelTableModel::addModelTable($data);

        if ($result) {
            $this->request->log_content = '添加模型数据成功';
            return tn_yes('添加模型数据成功');
        } else {
            $this->request->log_content = '添加模型数据失败';
            return tn_no('添加模型数据失败');
        }
    }

    /**
     * 编辑模型信息
     * @http put
     * @url /model_table/edit
     * @return \think\response\Json
     */
    public function editModelTable()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','cn_name','en_name','status','fields_data']);

        $result = ModelTableModel::editModelTable($data);

        if ($result) {
            $this->request->log_content = '编辑模型数据成功';
            return tn_yes('编辑模型数据成功');
        } else {
            $this->request->log_content = '编辑模型数据失败';
            return tn_no('编辑模型数据失败');
        }
    }

    /**
     * 更新模型信息
     * @http PUT
     * @url /model_table/update
     * @return \think\response\Json
     */
    public function updateModelTable()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = ModelTableModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新模型信息成功';
            return tn_yes('更新模型信息成功');
        }else {
            $this->request->log_content = '更新模型信息失败';
            return tn_no('更新模型信息失败');
        }
    }

    /**
     * 删除指定的模型信息
     * @http DELETE
     * @url /model_table/delete
     * @return \think\response\Json
     */
    public function deleteModelTable()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = ModelTableModel::delModelTable($data);

        if ($result) {
            $this->request->log_content = '删除模型信息成功';
            return tn_yes('删除模型信息成功');
        }else {
            $this->request->log_content = '删除模型信息失败';
            return tn_no('删除模型信息失败');
        }
    }
}