<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 13:53
 */
declare(strict_types = 1);

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\exception\SuperAdminException;
use app\common\exception\UrlNoPresenceException;
use app\admin\model\Admin as AdminModel;

class Admin extends BaseController
{
    /**
     * 管理员登陆
     * @http POST
     * @url /admin/login
     * @return \think\response\Json
     */
    public function login()
    {
        if (!$this->request->isPost()) {
            throw new UrlNoPresenceException();
        }

        $data = $this->request->post(['user_name','password']);

        $token = (string)AdminModel::login($data);

        return tn_yes('登陆成功', [
            'token' => $token
        ]);
    }

    /**
     * 管理员退出
     * @http POST
     * @url /admin/logout
     * @return \think\response\Json
     */
    public function logout()
    {
        if (!$this->request->isPost()) {
            throw new UrlNoPresenceException();
        }

        // 获取token的数据
        $token_data = $this->request->token_data;

        AdminModel::logout($token_data['uid']);

        return tn_yes('退出成功');
    }

    /**
     * 获取对应管理员的路由信息
     * @http GET
     * @url /admin/routes
     * @return \think\response\Json
     */
    public function routes()
    {
        // 获取token的数据
        $token_data = $this->request->token_data;

        $routes = AdminModel::getRoutes($token_data);

        return tn_yes('获取数据成功', $routes);
    }

    /**
     * 获取对应管理员的信息
     * @http GET
     * @url /admin/info
     * @return \think\response\Json
     */
    public function info()
    {
        // 获取token的数据
        $token_data = $this->request->token_data;

        $info = AdminModel::getInfo($token_data);

        return tn_yes('获取用户信息成功', $info);
    }

    /**
     * 获取管理员列表数据
     * @http GET
     * @url /admin/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        // 获取token的数据
        $token_data = $this->request->token_data;

        $data = AdminModel::getPaginationList($token_data,$params);

        return tn_yes('获取管理员列表数据成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取管理员信息
     * @http GET
     * @url /admin/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        // 获取token的数据
        $token_data = $this->request->token_data;

        if ($token_data['uid'] != 1 && $id == 1) {
            throw new SuperAdminException();
        }

        $data = AdminModel::getAdminByID($id);

        return tn_yes('获取管理员信息成功',['data' => $data]);
    }

    /**
     * 添加管理员
     * @http POST
     * @url /admin/add
     * @return \think\response\Json
     */
    public function addAdmin()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['user_name','nick_name','password','checkpass','role_id','gender'=>0,'avatar','introduction','status'=>0]);

        $result = AdminModel::addAdmin($data);

        if ($result) {
            $this->request->log_content = '添加管理员成功';
            return tn_yes('添加管理员成功');
        } else {
            $this->request->log_content = '添加管理员失败';
            return tn_no('添加管理员失败');
        }
    }

    /**
     * 编辑管理员
     * @http PUT
     * @url /admin/edit
     * @return \think\response\Json
     */
    public function editAdmin()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','user_name','nick_name','password','checkpass','role_id','gender'=>0,'avatar','introduction','status'=>0]);

        $data['token_data'] = $this->request->token_data;

        $result = AdminModel::editAdmin($data);

        if ($result) {
            $this->request->log_content = '编辑管理员成功';
            return tn_yes('编辑管理员成功');
        }else {
            $this->request->log_content = '编辑管理员失败';
            return tn_no('编辑管理员失败');
        }
    }

    /**
     * 更新管理员信息
     * @http PUT
     * @url /admin/update
     * @return \think\response\Json
     */
    public function updateAdmin()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AdminModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新管理员信息成功';
            return tn_yes('更新管理员信息成功');
        }else {
            $this->request->log_content = '更新管理员信息失败';
            return tn_no('更新管理员信息失败');
        }
    }

    /**
     * 删除指定的管理员信息
     * @http DELETE
     * @url /admin/delete
     * @return \think\response\Json
     */
    public function deleteAdmin()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = AdminModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除管理员信息成功';
            return tn_yes('删除管理员信息成功');
        }else {
            $this->request->log_content = '删除管理员信息失败';
            return tn_no('删除管理员信息失败');
        }
    }
}