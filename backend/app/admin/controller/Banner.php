<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 15:11
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Banner as BannerModel;

class Banner extends BaseController
{
    /**
     * 获取轮播图分页数据
     * @http get
     * @url /banner/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = BannerModel::getPaginationList($params);

        return tn_yes('获取轮播图分页信息成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取对应的轮播图信息
     * @http get
     * @url /banner/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id',0);

        $data = BannerModel::getDataWithID($id);

        return tn_yes('获取轮播图信息成功', ['data' => $data]);
    }

    /**
     * 获取指定轮播位下的轮播图数量
     * @http get
     * @url /banner/get_banner_count
     * @return \think\response\Json
     */
    public function getBannerCountByPosID()
    {
        $pos_id = $this->request->get('pos_id', 0);

        $count = BannerModel::getChildrenCount($pos_id, 'pos_id');

        return tn_yes('获取指定轮播位下的轮播图数量成功', ['count' => $count]);
    }

    /**
     * 添加轮播图信息
     * @http post
     * @url /banner/add
     * @return \think\response\Json
     */
    public function addBanner()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title','pos_id'=>0,'article_id'=>0,'image','sort','status'=>0]);

        $result = BannerModel::addBanner($data);

        if ($result) {
            $this->request->log_content = '添加轮播图成功';
            return tn_yes('添加轮播图成功');
        } else {
            $this->request->log_content = '添加轮播图失败';
            return tn_no('添加轮播图失败');
        }
    }

    /**
     * 编辑轮播图图信息
     * @http put
     * @url /banner/edit
     * @return \think\response\Json
     */
    public function editBanner()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','title','pos_id'=>0,'article_id'=>0,'image','sort','status'=>0]);

        $result = BannerModel::editBanner($data);

        if ($result) {
            $this->request->log_content = '编辑轮播图成功';
            return tn_yes('编辑轮播图成功');
        } else {
            $this->request->log_content = '编辑轮播图失败';
            return tn_no('编辑轮播图失败');
        }
    }

    /**
     * 更新轮播图信息
     * @http PUT
     * @url /banner/update
     * @return \think\response\Json
     */
    public function updateBanner()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = BannerModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新轮播图信息成功';
            return tn_yes('更新轮播图信息成功');
        }else {
            $this->request->log_content = '更新轮播图信息失败';
            return tn_no('更新轮播图信息失败');
        }
    }

    /**
     * 删除轮播图信息
     * @http delete
     * @url /banner/delete
     * @return \think\response\Json
     */
    public function deleteBanner()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = BannerModel::delByIDs($ids);

        if ($result) {
            $this->request->log_content = '删除轮播图成功';
            return tn_yes('删除轮播图成功');
        } else {
            $this->request->log_content = '删除轮播图失败';
            return tn_no('删除轮播图失败');
        }
    }
}