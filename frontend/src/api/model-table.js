import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getModelTableList(params) {
  return request({
    url: api_prefix + 'model_table/list',
    method: 'get',
    params
  })
}

export function getModelTableByID(id) {
  return request({
    url: api_prefix + 'model_table/get_id',
    method: 'get',
    params: { id }
  })
}

export function getModelTableAllName() {
  return request({
    url: api_prefix + 'model_table/get_all_name',
    method: 'get'
  })
}

export function getModelFieldsData(params) {
  return request({
    url: api_prefix + 'model_table/get_fields_data',
    method: 'get',
    params
  })
}

export function addModelTable(data) {
  return request({
    url: api_prefix + 'model_table/add',
    method: 'post',
    data
  })
}

export function editModelTable(data) {
  return request({
    url: api_prefix + 'model_table/edit',
    method: 'put',
    data
  })
}

export function updateModelTable(data) {
  return request({
    url: api_prefix + 'model_table/update',
    method: 'put',
    data
  })
}

export function deleteModelTable(ids) {
  return request({
    url: api_prefix + 'model_table/delete',
    method: 'delete',
    data: { ids }
  })
}
