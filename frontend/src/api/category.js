import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getCategoryTableTree(params) {
  return request({
    url: api_prefix + 'category/table_tree',
    method: 'get',
    params
  })
}

export function getCategoryTopNode() {
  return request({
    url: api_prefix + 'category/get_all_top',
    method: 'get'
  })
}

export function getCategoryAllNode() {
  return request({
    url: api_prefix + 'category/get_all',
    method: 'get'
  })
}

export function getCategoryChildrenCount(pid) {
  return request({
    url: api_prefix + 'category/get_children_count',
    method: 'get',
    params: { pid }
  })
}

export function getCategoryByID(id) {
  return request({
    url: api_prefix + 'category/get_id',
    method: 'get',
    params: { id }
  })
}

export function addCategory(data) {
  return request({
    url: api_prefix + 'category/add',
    method: 'post',
    data
  })
}

export function editCategory(data) {
  return request({
    url: api_prefix + 'category/edit',
    method: 'put',
    data
  })
}

export function updateCategory(data) {
  return request({
    url: api_prefix + 'category/update',
    method: 'put',
    data
  })
}

export function deleteCategory(ids) {
  return request({
    url: api_prefix + 'category/delete',
    method: 'delete',
    data: { ids }
  })
}
